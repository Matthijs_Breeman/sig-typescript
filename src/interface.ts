// Interfaces go here
export interface RandomNumber {
  id: string;
  date: string;
  numbers: number[];
}
