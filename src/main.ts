import { RandomNumber } from "./interface";
import needle from "needle";

const randomNumberAddress =
  "https://randomnumbergenerator-typescriptsig.azurewebsites.net/api/randomnumbergenerator?code=3VvfRSG9rwNkWTlPXCR8g0SADbhqf/axR3iDMnu2SvLIPND6ks/8LA==";
const numbersApiAddress = "http://numbersapi.com/";
const triviaApi =
  "https://randomnumbergenerator-typescriptsig.azurewebsites.net/api/NumericFactsDatabase?code=LbOqqn6XQNeHKxkI9Dd1DX7QtAUalVv1sF4ReBd/pBSdx0E1Fd6QAw==";

main();
async function main() {
  // get randomnumber generator
  await getRandomNumber();

  // Now use the randomNumber given to request trivia for this number
  // Make sure you request the json format, not the string format
  // triviaBody has an "any" type by default
  // this does not give any support in the ide, that can be improved

  // Now add your name to this triviaBody
  // And send it to the triviaApi
}

async function getRandomNumber() {
  // Note, in the next line you see the "await" keyword. This indicates that the code behind it is asynchronous
  // However the await keyword will ensure the code is executed before moving on to the next line.
  // If you ever encounter code being executed before you want it, consider the await and Promise keywords.
  const randomNumberRes = await needle("get", randomNumberAddress);
  const randomBody: RandomNumber = randomNumberRes.body;
  console.log("randomBody :>> ", randomBody);
}
